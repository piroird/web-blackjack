function getCards(){
    let res = [];
    for (let i = 1; i < 14; i++) {
      res.push(`${i}-carreau.jpg`);
      res.push(`${i}-coeur.jpg`);
      res.push(`${i}-pique.jpg`);
      res.push(`${i}-trefle.jpg`);
    }
    return res;
}

function extractNb(card){
  return Number(card.split("-")[0]);
}

function pickCard(deck){
  let nb = Math.floor(Math.random()*cards.length);
  let c = cards.splice(nb, 1)[0];
  deck.push(c);
  return c;
}

function calcPoints(deck){
  let totalPts = 0;
  let nbAs = 0;
  for(let card of deck){ // Ajout des points
    let pts = Math.min(extractNb(card), 10);
    if(pts == 1){
      ++nbAs;
      totalPts += 11;
    }
    else{
      totalPts += pts;
    }
  }
  while(nbAs-- > 0 && totalPts > 21){ // Vérification des As
      totalPts -= 10;
  }
  return totalPts;
}

function start(){
  mise = demanderMise();
  if(mise == null) return;
  jetons -= mise;
  document.getElementById("mise").innerText = mise;

  cards = getCards();
  deckJ = [];
  deckC = [];
  pickCard(deckJ);
  pickCard(deckC);
  pickCard(deckJ);
  pickCard(deckC);

  updateDeckJ();
  updateDeckC(false);

  if(calcPoints(deckJ) == 21){
    jetons += (mise*2);
    openModalP(`BLACKJACK !\nVous venez de gagner ${mise*2} jetons.`);
    if(jetons > maxi)
      maxi = jetons;
    setVisible("start", true);
  }
  else{
    setVisible("start", false);
    setVisible("continue", true);
    if(mise < jetons)
      setVisible("double", true);
    setVisible("stop", true);
  }
  updateJetons();
}

function demanderMise(){
  while (true) {
    let rep = window.prompt(`Montant de la mise ?\n    Maximum: ${jetons}`);
    if(rep == null){
      return null;
    }
    else if(rep != ""){
      let nb = parseInt(rep);
      if(!isNaN(nb) && nb >= 1 && nb <= jetons)
        return nb;
    }
  }
}

function init(cookie){
  if(cookie){
    let jetonsC = getCookie("jetons");
    jetons = (jetonsC == null ? 5000 : jetonsC);
    let maxiC = getCookie("maxi");
    maxi = (maxiC == null ? 5000 : maxiC);
  }
  else{
    jetons = 5000;
    maxi = 5000;
  }
  mise = 0;
  updateJetons();
  btn = {"start": document.getElementById('start'),
         "continue": document.getElementById('continue'),
         "double": document.getElementById('double'),
         "stop": document.getElementById('stop')}
  setVisible("start", true);
}

function continueP(){
  setVisible("double", false);
  pickCard(deckJ);
  updateDeckJ();
  if(calcPoints(deckJ)>21){
    setVisible("continue", false);
    setVisible("stop", false);
    calcWin();
  }
}

function double(){
  jetons -= mise;
  mise*=2;
  document.getElementById("mise").innerText = mise;
  pickCard(deckJ);
  updateDeckJ();
  updateJetons();

  if(calcPoints(deckJ)>21)
    calcWin();
  else
    stop();
}

function stop(){
  setVisible("continue", false);
  setVisible("double", false);
  setVisible("stop", false);

  updateDeckC(true);
  let ptsJ = calcPoints(deckJ);
  k = setInterval(() => {
    let ptsC = calcPoints(deckC);
    if(ptsC <= ptsJ && ptsC < 21){
      pickCard(deckC);
      updateDeckC(true);
    }
    else{
      clearInterval(k);
      calcWin();
    }
  }, 800);
}

function calcWin(){
  setVisible("start", true);
  let ptsJ = calcPoints(deckJ);
  let ptsC = calcPoints(deckC);
  if (ptsJ == ptsC && deckJ.length == deckC.length) {
    jetons += mise;
    openModalP(`Égalité, vous gardez vos ${mise} jetons.`);
  }
  else if (ptsC <= 21 && (ptsC > ptsJ || (ptsC == ptsC && deckJ.length > deckC.length))) {
    openModalP(`Désolé vous avez perdu votre mise de ${mise} !`);
    if(jetons == 0)
      openModalEnd();
  }
  else {
    jetons += Math.floor(mise*1.5);
    openModalP(`Victoire ! Vous gagnez ${mise*.5} jetons.`);
    if(jetons > maxi)
      maxi = jetons;
  }
  updateJetons();
}

function updateJetons(){
  document.getElementById("jetons").innerText = jetons;
  document.getElementById("maxi").innerText = maxi;
  setCookie("jetons", jetons);
  setCookie("maxi", maxi);
}

function updateDeckJ(){
  let div = document.getElementById("deckJ");
  div.innerHTML = "<h2>Vos cartes</h2>";
  for (let carte of deckJ) {
    let img = document.createElement("img");
    img.src = `images/cartes/${carte}`;
    img.alt = carte;
    div.appendChild(img);
  }
  document.getElementById('scoreJ').innerText = calcPoints(deckJ);
}

function updateDeckC(visible){
  let div = document.getElementById("deckC");
  div.innerHTML = "<h2>Les cartes du croupier</h2>";
  if(visible){
    for (let carte of deckC) {
      let img = document.createElement("img");
      img.src = `images/cartes/${carte}`;
      img.alt = carte;
      div.appendChild(img);
    }
    document.getElementById('scoreC').innerText = calcPoints(deckC);
  }
  else{
    for(let i=0; i<deckC.length; ++i){
      let img = document.createElement("img");
      img.src = `images/paquet.jpg`;
      img.alt = "Carte retournée";
      div.appendChild(img);
    }
    document.getElementById('scoreC').innerText = "";
  }
}

function openModalP(txt){
  let modal = document.getElementById("modalP");
  modal.querySelector("p").innerText = txt;
  modal.style.visibility = "visible";
}

function closeModalP(){
  let modal = document.getElementById("modalP");
  modal.style.visibility = "hidden";
}

function openModalEnd(){
  let modal = document.getElementById("modalEnd");
  modal.querySelector("span#maxi").innerText = maxi;
  let record = getCookie("record");
  if(record != null)
    modal.querySelector("span#record").innerText =
        `\nVotre précédent record était de ${record}.`;
  modal.style.visibility = "visible";
  if(record == null || record < maxi){
    setCookie("record", maxi);
  }
}

function closeModalEnd(){
  let modal = document.getElementById("modalEnd");
  modal.style.visibility = "hidden";
  // Faire sauvegarde
  let record = getCookie("record");
  if(record != null && record < maxi)
    setCookie("record", maxi);
  init(false);
}

function setVisible(btnName, visible) {
  btn[btnName].style.visibility = visible ? "visible" : "hidden";
}

function setCookie(name, value) {
  // cookie sauvegardé un an
  document.cookie = `${name}=${value};max-age=31536000;path=/;SameSite=strict`;
}

function getCookie(name) {
  let cookies = document.cookie.split("; ");
  for(let el of cookies) {
    let c = el.split("=");
    if(c[0] == name)
      return Number(c[1]);
  }
  return null;
}

window.onload = init(true);
